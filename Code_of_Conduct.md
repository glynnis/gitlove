## Code of Conduct
(adapted from the [Rust code of conduct][rust-coc])

We are committed to providing a friendly, safe and welcoming environment
for all, regardless of gender, sexual orientation, disability, ethnicity,
religion, or similar personal characteristic.  As such, all interactions
on Gitlove are moderated.

- Please avoid using overtly sexual usernames or other usernames that
  might detract from a friendly, safe and welcoming environment for all.

- Please be kind and courteous. There's no need to be mean or rude.

- Respect that people have differences of opinion and that every design
  or implementation choice carries a trade-off and numerous costs. There
  is seldom a right answer.

- Please keep unstructured critique of others' work to a minimum. If
  you have solid ideas you want to experiment with, make a fork and see
  how it works.

- Do not insult, demean or harass anyone. That is not welcome
  behaviour. We interpret the term "harassment" as including the
  definition in the [Citizen Code of Conduct][citizen-coc]; if you have
  any lack of clarity about what might be included in that concept,
  please read their definition. In particular, we don't tolerate behavior
  that excludes people in socially marginalized groups.

- Private harassment is also unacceptable. No matter who you are, if
  you feel you have been or are being harassed or made uncomfortable by
  a community member, please contact $WHO immediately. Whether you're a
  regular contributor or a newcomer, we care about making this community
  a safe place for you and we've got your back.

- Likewise any spamming, trolling, flaming, baiting or other
  attention-stealing behaviour is not welcome.

## Moderation

Gitlove moderators are chosen by $PROCESS.

All moderator actions are publicly indexed.

To appeal the choice of a particular moderator, $TODO.

To flag an interaction for moderator attention, $TODO.

To report harrassment through private channels, $TODO.

To report a moderator, $TODO.

### Moderator actions

If a piece of content is found to be in violation of the code of
conduct, that content will be marked with the moderation action taken
(for example, "@example was given a 1-week probation for this comment").
Particularly egregious content will be edited out.

- probation: while your account is in probation, you may push to any
  repository you have access to, but you may not add comments or open
  merge requests anywhere on the site.  This is generally a temporary
  measure.
- ban: while your account is banned, site login and git access are
  disabled for your account.
- permaban: if your account is permabanned, site login and git access
  will be disabled, and connections originating from your IP will not
  be able to create new accounts.
- block: if your account is blocked by another user, you will not be able
  to comment on any part of any of their repositories, nor open issues
  or merge requests.
- lock: if a repository, issue, merge request, or comment thread is
  locked, no one will be able to add additional comments or push commits
  to the repository.  When a repository is locked, all its issues,
  merge requests, and comment threads are locked as well.

To appeal a moderator action, you may $TODO.

[rust-coc]: https://github.com/mozilla/rust/wiki/Note-development-policy#Conduct "Rust Code of Conduct"
[citizen-coc]: http://citizencodeofconduct.org/ "Citizen Code of Conduct"
