# The idea behind Gitlove

There really are few alternatives to Github. Bitbucket has its own 'bucket' of issues. Who knows when Google is going to axe code.google.com. Sourceforge could work if you can get past the UI. Gitlab.com does have free hosting as well as paid. If we're going to use Gitlab there is really no reason why you can't, and no reason I can see to avoid gitlab.com either.

My initial idea is that with all the recent uproar, why don't we as a community do something? Surely, we can scrap together enough human, and hardware resources to do this. We can build a site where there is a clear Code of Conduct, and Process to deal with harassment. Where transparency and volunteers work to build a great community.

The various groups that have popped up in the past few years (Railsbridge, Women Who Code, Black Girls Code, Transh4ck) not to mention all the Hacker Academies are adding to our developer pool. Many of those groups introduce newcomers to the field. Many of those groups need a place to host their code. Many groups work on teaching Rails.

Gitlab is written in Rails. We have classes to teach people Rails. We can submit Merge Requests to Gitlab.

Seems like we can take that energy and pool it into a common good.

I'd love for groups that do Systems Administration to join in here too!

There are features that Github is lacking that make some community work difficult. eg;

 - blocking/ignoring certain people
 - ability to close a topic
 - clear visibility of Code of Conduct on project pages
 - ability to accept a Code of Conduct before getting access to a project
 - I'm sure there are others, please mention them.

At this point, we have gitlove.io registered. We need a place to host gitlab. It has modest requirements: http://doc.gitlab.com/ce/install/requirements.html (as well as MySQL).

I of course have no issue with people moving directly to Gitlab. Do it!

Lots of things to work out here still!

Please leave feedback, constructive criticism. Please keep negative remarks, and can't do attitudes.

[Issues](https://gitlab.com/gitlove/gitlove/issues)
[Code of Conduct](https://gitlab.com/gitlove/gitlove/blob/master/Code_of_Conduct.md)
[Contributing](https://gitlab.com/gitlove/gitlove/blob/master/Contributing.md)
